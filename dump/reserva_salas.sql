-- phpMyAdmin SQL Dump
-- version 4.0.4.2
-- http://www.phpmyadmin.net
--
-- Máquina: localhost
-- Data de Criação: 19-Dez-2016 às 01:20
-- Versão do servidor: 5.6.13
-- versão do PHP: 5.4.17

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de Dados: `reserva_salas`
--
CREATE DATABASE IF NOT EXISTS `reserva_salas` DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci;
USE `reserva_salas`;

-- --------------------------------------------------------

--
-- Estrutura da tabela `booking`
--

CREATE TABLE IF NOT EXISTS `booking` (
  `id` int(10) unsigned NOT null AUTO_INCREMENT,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT null,
  `room_id` int(10) unsigned NOT null,
  `user_id` int(10) unsigned NOT null,
  `date_ini` datetime NOT null,
  `date_fim` datetime NOT null,
  PRIMARY KEY (`id`,`room_id`,`user_id`),
  UNIQUE KEY `booking_UNIQUE` (`id`),
  KEY `fk_booking_room_idx` (`room_id`),
  KEY `fk_booking_user_idx` (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=24 ;

--
-- Extraindo dados da tabela `booking`
--

INSERT INTO `booking` (`id`, `description`, `room_id`, `user_id`, `date_ini`, `date_fim`) VALUES
(16, 'teste', 1, 11, '2030-05-10 11:50:00', '2030-05-10 12:50:00'),
(21, 'teste', 1, 11, '2030-05-10 10:49:00', '2030-05-10 11:49:00'),
(22, 'Teste', 1, 11, '2030-05-10 12:51:00', '2030-05-10 13:51:00');

-- --------------------------------------------------------

--
-- Estrutura da tabela `room`
--

CREATE TABLE IF NOT EXISTS `room` (
  `id` int(10) unsigned NOT null AUTO_INCREMENT,
  `label` varchar(45) COLLATE utf8_unicode_ci NOT null,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT null,
  PRIMARY KEY (`id`),
  UNIQUE KEY `room_UNIQUE` (`id`),
  UNIQUE KEY `label_UNIQUE` (`label`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=11 ;

--
-- Extraindo dados da tabela `room`
--

INSERT INTO `room` (`id`, `label`, `description`) VALUES
(1, 'Sala1', 'Descrição da sala1'),
(2, 'Sala2', 'Descrição da sala2'),
(3, 'Sala3', 'Descrição da sala3'),
(4, 'Sala4', 'Descrição da sala4'),
(5, 'Sala5', 'Descrição da sala5'),
(6, 'Sala6', 'Descrição da sala6'),
(7, 'Sala7', 'Descrição da sala7'),
(8, 'Sala8', 'Descrição da sala8'),
(9, 'Sala9', 'Descrição da sala9'),
(10, 'Sala10', 'Descrição da sala10');

-- --------------------------------------------------------

--
-- Estrutura da tabela `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `id` int(10) unsigned NOT null AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT null,
  `username` varchar(20) COLLATE utf8_unicode_ci NOT null,
  `hash` varchar(255) COLLATE utf8_unicode_ci NOT null,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_UNIQUE` (`id`),
  UNIQUE KEY `username_UNIQUE` (`username`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=12 ;

--
-- Extraindo dados da tabela `user`
--

INSERT INTO `user` (`id`, `name`, `username`, `hash`) VALUES
(1, 'Usuario1', 'user1', 'e8d95a51f3af4a3b134bf6bb680a213a'),
(2, 'Usuario2', 'user2', 'e8d95a51f3af4a3b134bf6bb680a213a'),
(3, 'Usuario3', 'user3', 'e8d95a51f3af4a3b134bf6bb680a213a'),
(4, 'Usuario4', 'user4', 'e8d95a51f3af4a3b134bf6bb680a213a'),
(5, 'Usuario5', 'user5', 'e8d95a51f3af4a3b134bf6bb680a213a'),
(6, 'Usuario6', 'user6', 'e8d95a51f3af4a3b134bf6bb680a213a'),
(7, 'Usuario7', 'user7', 'e8d95a51f3af4a3b134bf6bb680a213a'),
(8, 'Usuario8', 'user8', 'e8d95a51f3af4a3b134bf6bb680a213a'),
(9, 'Usuario9', 'user9', 'e8d95a51f3af4a3b134bf6bb680a213a'),
(10, 'Usuario10', 'user10', 'e8d95a51f3af4a3b134bf6bb680a213a'),
(11, 'Administrador', 'admin', '21232f297a57a5a743894a0e4a801fc3');

--
-- Constraints for dumped tables
--

--
-- Limitadores para a tabela `booking`
--
ALTER TABLE `booking`
  ADD CONSTRAINT `fk_booking_room` FOREIGN KEY (`room_id`) REFERENCES `room` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_booking_user1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
