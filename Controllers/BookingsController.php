<?php

namespace Controllers;

use Models\Booking;
use Models\Room;
use \Exception;

class BookingsController extends Controller
{
    public static function list()
    {
        $orderBy = isset($_GET['orderby'])?$_GET['orderby']:null;
        $bookings = Booking::all($orderBy);
        view('bookings', ['bookings' => $bookings]);
    }

    public static function form()
    {
        $id = isset($_GET['id']) ? $_GET['id'] : null;
        $errors = [];
        $booking = new Booking($id);
        $rooms = Room::all();
        view('booking-form', ['booking' => $booking, 'rooms' => $rooms, 'errors' => $errors]);
    }

    public static function save()
    {
        $id = isset($_GET['id']) ? $_GET['id'] : null;
        $booking = new Booking($id);

        $errors = array();

        if (isset($_POST['form-submitted'])) {
            $booking->setUserId($_SESSION['user_id']);
            $booking->setRoomId(     isset($_POST['room_id']      ) ? $_POST['room_id']      : null);
            $booking->setDescription(isset($_POST['description'] ) ? $_POST['description'] : null);
            $booking->setDateIni(    isset($_POST['date_ini']     ) ? date('Y-m-d H:i:s', strtotime($_POST['date_ini'])) : null);
            $booking->setDateFim(date('Y-m-d H:i:s', strtotime($_POST['date_ini'])+60*60));

            //echo "<script>alert(\"".$booking->getDateIni()."\n".$booking->getDateFim()."\")</script>";
            
            try {
                $booking->verificaReserva();
                $booking->save();
                header("Location: /bookings");
                return;
            } catch (Exception $e) {
                $errors[] = $e->getMessage();
            }
        }
        
        $rooms = Room::all();
        view('booking-form', ['booking' => $booking, 'rooms' => $rooms, 'errors' => $errors]);
    }
    
    public static function delete()
    {
        $id = isset($_GET['id']) ? $_GET['id'] : null;
        if (!$id) {
            throw new Exception('Internal error.');
        }
        try {
            $booking = new Booking($id);
            $booking->erase();
        } catch (Exception $e) {
            throw new Exception('Reseva não encontrada.');
        }

        header("Location: /bookings");
    }
    
    public static function show()
    {
        $id = isset($_GET['id']) ? $_GET['id'] : null;
        if (!$id) {
            throw new Exception('Internal error.');
        }
        try {
            $booking = new Booking($id);
        } catch (Exception $e) {
            throw new Exception('Reserva não encontrada.');
        }

        view('booking', ['booking' => $booking]);
    }
    
    public function showError($title, $message)
    {
        view('error', ['title'=> $title, 'message' => $message]);
    }

/*
    public function handleRequest()
    {
        $action = isset($_GET['ac'])?$_GET['ac']:null;

        try {
            if (!$action || $action == 'listar') {
                $this->list();
            } elseif ($action == 'novo' || $action == 'editar') {
                $this->saveBooking();
            } elseif ( $action == 'excluir') {
                $this->deleteBooking();
            } elseif ($action == 'exibir') {
                $this->showBooking();
            } else{
                $this->showError("Página não encontrada", "Página para a ação '".$action."' não foi encontrada!");
            }
        } catch (Exception $e) {
            $this->showError("Application error", $e->getMessage());
        }
    }
    
*/

}