<?php

namespace Controllers;

use Models\Room;
use Models\ValidationException;
use \Exception;

class RoomsController extends Controller
{
    
    public static function list()
    {
        $orderBy = isset($_GET['orderby'])?$_GET['orderby']:null;
        $rooms = Room::all($orderBy);
        view('rooms', ['rooms' => $rooms]);
    }

    public static function form()
    {
        $id = isset($_GET['id']) ? $_GET['id'] : null;
        $errors = [];
        $room = new Room($id);
        view('room-form', ['room' => $room, 'errors' => $errors]);
    }
    
    public static function delete()
    {
        $id = isset($_GET['id'])?$_GET['id']:null;
        if (!$id) {
            throw new Exception('Internal error.');
        }
        try {
            $room = new Room($id);
            $room->erase();
        } catch (Exception $e) {
            throw new Exception('Sala não encontrado.');
        }
        header("Location /rooms");
    }
    
    public static function show()
    {
        $id = isset($_GET['id'])?$_GET['id']:null;
        if ( !$id ) {
            throw new Exception('Internal error.');
        }
        try {
            $room = new Room($id);
        } catch (Exception $e) {
            throw new Exception('Sala não encontrado.');
        }

        view('room', ['room' => $room]);
    }

    public static function save()
    {
        $id = isset($_GET['id'])?$_GET['id']:null;
        $room = new Room($id);
        $errors = array();

        if (isset($_POST['form-submitted'])) {
            $room->setLabel(    isset($_POST['label']    ) ? $_POST['label']     : null);
            $room->setDescription(isset($_POST['description']) ? $_POST['description'] : null);
            try {
                $room->save();
                header("Location: /rooms");
                return;
            } catch (ValidationException $e) {
                $errors = $e->getErrors();
            }
        }
        
        view('room-form', ['room' => $room, 'errors' => $errors]);
    }
/*
    public function handleRequest()
    {
        $action = isset($_GET['ac'])?$_GET['ac']:null;

        try {
            if (!$action || $action == 'listar') {
                $this->list();
            } elseif ($action == 'novo' || $action == 'editar') {
                $this->saveRoom();
            } elseif ( $action == 'excluir') {
                $this->deleteRoom();
            } elseif ($action == 'exibir') {
                $this->showRoom();
            } else {
                $this->showError("Página não encontrada", "Página para a ação '".$action."' não foi encontrada!");
            }
        } catch (Exception $e) {
            $this->showError("Application error", $e->getMessage());
        }
    }
*/
}