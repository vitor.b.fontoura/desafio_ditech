<?php

namespace Controllers;

use Models\User;
use \Exception;
use Models\ValidationException;

class LoginController extends Controller
{
    public function handleRequest()
    {
        $action = isset($_GET['ac'])?$_GET['ac']:null;
        try {
            if (!$action) {
                $this->form();
            } elseif ( $action == 'logout') {
                $this->logout();
            } else {
                $this->showError("Página não encontrada", "Página para a ação '".$action."' não foi encontrada!");
            }
        } catch (Exception $e) {
            $this->showError("Application error", $e->getMessage());
        }
    }

    public function logout()
    {
        unset($_SESSION['user']);
        unset($_SESSION['loggedin']);
        session_destroy();
        header("Location: /login");
        return;
    }

    public function login()
    {
        $errors = array();
        if (isset($_POST['form-submitted'])) {
            try {
                $id = User::isRegistered($_POST['username'], $_POST['password']);
            } catch (ValidationException $e) {
                $errors = $e->getErrors();
                view('login', ['errors' => $errors]);
                return;
            }
            $user = new User($id);
            $_SESSION['user_id'] = $user->getId();
            $_SESSION['loggedin'] = true;
            echo "Aqui de boas";
            header("Location: /users");
            return;
        }
    }
    
    public function form()
    {
        view('login');
    }
}