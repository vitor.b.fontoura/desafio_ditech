<?php

namespace Controllers;

use Models\User;
use Models\ValidationException;
use \Exception;

class UsersController extends Controller
{

    public static function list()
    {
        $orderBy = isset($_GET['orderby'])?$_GET['orderby']:null;
        $users = User::all($orderBy);
        view('users', ['users' => $users]);
    }

    public static function form()
    {
        $id = isset($_GET['id']) ? $_GET['id'] : null;
        $errors = [];
        $user = new User($id);
        view('user-form', ['user' => $user, 'errors' => $errors]);
    }

    public static function save()
    {
        $id = isset($_POST['id']) ? $_POST['id'] : null;
        
        $user = new User($id);

        $user->setName(    isset($_POST['name']    ) ? $_POST['name']     : null);
        $user->setUsername(isset($_POST['username']) ? $_POST['username'] : null);

        if ($user->getId()!=null) {
            if ($user->getHash() != \Util::createHash($_POST['oldpassword'])) {
                $errors[] = "A senha anterior não corresponde.";
            }
        }

        if ($_POST['password'] != $_POST['passwordconf']) {
            $errors[] = "A confirmação não corresponde.";
            view('user-form', ['user' => $user, 'errors' => $errors]);
            return;
        }
        
        $user->setPassword(isset($_POST['password']) ? $_POST['password'] : null);

        try {
            $user->save();
            header("Location: /users");
            return;
        } catch (ValidationException $e) {
            $errors = $e->getErrors();
        }
    }
    
    public static function delete()
    {
        $id = isset($_GET['id'])?$_GET['id']:null;
        if (!$id) {
            throw new Exception('Internal error.');
        }
        try {
            $user = new User($id);
            $user->erase();
        } catch (Exception $e) {
            throw new Exception('Usuário não encontrado.');
        }
        header("Location: /users");
        //$this->redirect('index.php');
    }
/*
    public function handleRequest()
    {
        $action = isset($_GET['ac'])?$_GET['ac']:null;

        try {
            if (!$action || $action == 'listar') {
                $this->list();
            } elseif ($action == 'novo' || $action == 'editar') {
                $this->saveUser();
            } elseif ( $action == 'excluir') {
                $this->deleteUser();
            } elseif ($action == 'exibir') {
                $this->showUser();
            } else {
                $this->showError("Página não encontrada", "Página para a ação '".$action."' não foi encontrada!");
            }
        } catch (\Exception $e) {
            $this->showError("Application error", $e->getMessage());
        }
    }

    public function saveUser()
    {
        $id = isset($_GET['id']) ? $_GET['id'] : null;

        $user = new User($id);

        $errors = array();

        if (isset($_POST['form-submitted'])) {
            $user->setName(    isset($_POST['name']    ) ? $_POST['name']     : null);
            $user->setUsername(isset($_POST['username']) ? $_POST['username'] : null);

            if ($user->getId()!=null) {
                if ($user->getHash() != \Util::createHash($_POST['oldpassword'])) {
                    $errors[] = "A senha anterior não corresponde.";
                    //view('user-form', ['user' => $user]);
                    //return;
                }
            }

            if ($_POST['password'] != $_POST['passwordconf']) {
                $errors[] = "A confirmação não corresponde.";
                view('user-form', ['user' => $user, 'errors' => $errors]);
                return;
            }
            
            $user->setPassword(isset($_POST['password']) ? $_POST['password'] : null);

            try {
                $user->save();
                $this->redirect('index.php');
                return;
            } catch (ValidationException $e) {
                $errors = $e->getErrors();
            }
        }
        
        view('user-form', ['user' => $user]);
    }

    public function showUser()
    {
        $id = isset($_GET['id'])?$_GET['id']:null;
        if ( !$id ) {
            throw new Exception('Internal error.');
        }
        try {
            $user = new User($id);
        } catch (Exception $e) {
            throw new Exception('Usuário não encontrado.');
        }

        view('user', ['user' => $user]);
    }
*/
}