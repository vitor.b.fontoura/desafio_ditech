<?php

namespace Controllers;

abstract class Controller
{
    public function redirect($location)
    {
        header("Location: {$location}");
        return;
        $op = $_REQUEST['op'] ?? "Users";
        header("Location: {$location}.php?op={$op}");
        echo $location;
    }

    public function showError($title, $message)
    {
        view('error', ['title'=> $title, 'message' => $message]);
    }   
}