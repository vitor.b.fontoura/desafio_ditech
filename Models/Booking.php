<?php

namespace Models;

use \DB;
use \Exception;

class Booking extends Model
{
	protected $id;
	protected $user_id;
	protected $room_id;
	protected $date_ini;
	protected $date_fim;
	protected $description;

	public function setId($id)
	{
		$this->id = $id;
	}

	public function getId()
	{
		return $this->id;
	}

	public function setDescription($description)
	{
		$this->description = $description;
	}

	public function getDescription()
	{
		return $this->description;
	}

	public function getUser()
	{
		if (empty($this->user)) {
			if (isset($this->user_id)) {
				$this->user = new User($this->user_id);
			} else {
				$this->user = new User();
			}
		}
		return $this->user;
	}

	public function setUser($user)
	{
		$this->user = $user;
		$this->user_id = $this->user->getId();
	}

	public function getRoom()
	{
		if (empty($this->room)) {
			$this->room = new Room($this->room_id);
		}
		return $this->room;
	}

	public function setRoom($room)
	{
		$this->room = $room;
		$this->id = $this->room->getId();
	}

	public function getDateIni()
	{
		return $this->date_ini;
	}

	public function setDateIni($date_ini)
	{
		$this->date_ini = $date_ini;
	}

	public function getDateFim()
	{
		return $this->date_fim;
	}

	public function setDateFim($date_fim)
	{
		$this->date_fim = $date_fim;
	}

	public function setUserId($id)
	{
		$this->user_id = $id;
		$this->user = new User($id);
	}

	public function setRoomId($id)
	{
		$this->room_id = $id;
		$this->room = new Room($id);
	}

	public function verificaReserva()
	{
		$database = DB::instance();
		$database->query("SELECT * FROM booking WHERE user_id = :user_id AND ((date_fim >= :date_ini AND date_ini <= :date_ini) OR (date_ini <= :date_fim AND date_fim >= :date_fim))");
		//$database->query("SELECT * FROM booking WHERE user_id = :user_id AND ((date_fim >= :date_ini) OR (date_ini <= :date_fim))");
		$database->bind(":user_id", $this->user_id);
		$database->bind(":date_ini", $this->date_ini);
		$database->bind(":date_fim", $this->date_fim);
		$database->execute();
		if ($database->rowCount() > 0) {
			throw new Exception('Existe conflito de reservas para o usuário.');
		}

		$database->query("SELECT * FROM booking WHERE room_id = :room_id AND ((date_fim >= :date_ini AND date_ini <= :date_ini) OR (date_ini <= :date_fim AND date_fim >= :date_fim))");
		$database->bind(":room_id", $this->room_id);
		$database->bind(":date_ini", $this->date_ini);
		$database->bind(":date_fim", $this->date_fim);
		$database->execute();
		if ($database->rowCount() > 0) {
			throw new Exception('Existe conflito de reservas para a sala selecionada.');
		}
	}
}