<?php

namespace Models;

interface ModelContract
{
	public function save();
	public function load($id);
	public function erase();
	public function clear();
}