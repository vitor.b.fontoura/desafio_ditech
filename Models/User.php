<?php

namespace Models;

use \DB;
use \Exception;
use \Util;

class User extends Model
{
	protected $id;
	protected $name;
	protected $username;
	protected $hash;

	public function setId($id)
	{
		$this->id = $id;
	}

	public function getId()
	{
		return $this->id;
	}

	public function setName($name)
	{
		$this->name = $name;
	}

	public function getName()
	{
		return $this->name;
	}

	public function setUsername($username)
	{
		$this->username = $username;
	}

	public function getUsername()
	{
		return $this->username;
	}

	public function setPassword($password)
	{
		$this->hash = Util::createHash($password);
	}

	public function setHash($hash)
	{
		$this->hash = $hash;
	}

	public function getHash()
	{
		return $this->hash;
	}

	public static function isRegistered($username, $password)
	{
		$database = DB::instance();
		$database->query("SELECT id, name, username, hash FROM user WHERE username = :username");
		$database->bind(":username", $username);

		$cols = $database->single();

		if ($database->RowCount() == 0) {
			throw new Exception("Usuário não cadastrado.");
		} elseif ($cols["hash"] <> Util::createHash($password)) {
			throw new Exception("Senha incorreta.");
		} else {
			return $cols["id"];
		}
	}
}