<?php

namespace Models;

use \DB;

class Model implements ModelContract
{
    public function __construct($id = null)
    {
    	if ($id)
    		$this->load($id);
    }

    private static function insert() : string
    {
        $table = static::table();
        $properties = static::properties();

        return "INSERT INTO {$table}(" . implode(", ", $properties) . ")VALUES(:" . implode(", :", $properties) . ")";
    }

    private static function update() : string
    {
        $table = static::table();
        $properties = static::properties();

        $sql = "UPDATE {$table} SET ";
        $fields = [];
        foreach ($properties as $property) {
            $fields[] = "{$property} = :{$property} ";
        }
        $sql .= implode(", ", $fields) . "WHERE id = :id";
        return $sql;
    }

    private static function delete() : string
    {
        $table = static::table();
        return "DELETE FROM {$table} WHERE id = :id";
    }

    private static function single() : string
    {
        $table = static::table();
        return "SELECT * FROM {$table} WHERE id = :id";
    }

    private static function select() : string
    {
        $table = static::table();
        return "SELECT * FROM {$table}";
    }

    private static function table()
    {
        return strtolower((new \ReflectionClass(static::class))->getShortName());
    }

    private static function properties()
    {
        $refProps = (new \ReflectionClass(static::class))->getProperties();
        $properties = [];
        foreach ($refProps as $prop) {
            if (!$prop instanceof \stdClass && $prop->name != "id")
                $properties[] = $prop->name;
        }

        return $properties;
    }

    public function load($id)
    {
        $database = DB::instance();
		$database->query(self::single());
		$database->bind(":id", $id);
		$cols = $database->single();

		if (!isset($cols["id"])) {
			throw new \Exception("Objeto não encontrado.");
        }
        foreach ($cols as $prop => $val) {
            $this->$prop = utf8_decode($val);
        }
    }
    
    public function save()
    {
		$database = DB::instance();
        $database->beginTransaction();

		if (isset($this->id)) {
            $database->query(self::update());
            $this->bindProperties($database);
            $database->bind(":id", $this->id);
            $database->execute();
		} else {
            $database->query(self::insert());
            $this->bindProperties($database);
			$database->execute();
			$this->id = $database->lastInsertId();
        }
		$database->endTransaction();
    }

    private function bindProperties(&$database)
    {
        foreach (self::properties() as $property) {
            $database->bind(":{$property}", utf8_encode($this->$property));
        }
    }

    public function clear()
    {
        foreach ($this->properties as $property) {
            unset($this->$property);
        }
        unset($this->id);
    }
    
    public function erase()
    {
        if (isset($this->id)) {
			$database = DB::instance();
			$database->beginTransaction();
			$database->query(self::delete());
			$database->bind(":id",$this->id);
			$database->execute();
			$database->endTransaction();
		}
		$this->clear();
    }

    public static function all($orderBy = null)
    {
        $database = DB::instance();
        if (!isset($orderBy)) {
			$orderBy = "id";
        }

		$database->query(self::select() . " ORDER BY ".$orderBy);
        $database->execute();

		$rows = $database->resultset();
        $count = $database->rowCount();

        $items = array();
        
		if ($count!=0) {
			for ($i=0;$i<$count;$i++) {
                $item = new static();
                $item->id = $rows[$i]['id'];
                foreach (self::properties() as $prop) {
                    $item->$prop = utf8_decode($rows[$i][$prop]);
                }
                $items[$i] = $item;
			}
        }

		return $items;
    }
}