<?php

use Controllers\BookingsController;
use Controllers\LoginController;
use Controllers\RoomsController;
use Controllers\UsersController;

//error_reporting(E_ALL);
//ini_set("display_errors", 1);

require_once "autoload.php";

date_default_timezone_set("America/Sao_Paulo");
session_start();

require_once '../Helpers/Database.php';
require_once '../Helpers/Util.php';
require_once '../Helpers/storage.php';
require_once '../Helpers/DB.php';

require_once '../Helpers/RequestContract.php';
require_once '../Helpers/Request.php';
require_once '../Helpers/Router.php';

$request = new Request();
$router = new Router($request);

//Error Pages
$router->get('/404', function() { view("404"); });
$router->get('/405', function() { view("405"); });

// Auth
if (empty($_SESSION['loggedin']) || !$_SESSION['loggedin']) {
	if ($request->requestUri !== '/login') {
		$router->redirect('login');
	}
} else {
	if ($request->requestUri == '/login') {
		$router->redirect('users');
	}
	if ($request->requestUri == '/') {
		$router->redirect('users');
	}
}

$router->get('/login', [LoginController::class, 'form']);
$router->post('/login', [LoginController::class, 'login']);
$router->get('/logout', [LoginController::class, 'logout']);

// Users
$router->get('/users', [UsersController::class, 'list']);
$router->get('/user', [UsersController::class, 'form']);
$router->post('/user', [UsersController::class, 'save']);
$router->get('/user/delete', [UsersController::class, 'delete']);

// Rooms
$router->get('/rooms', [RoomsController::class, 'list']);
$router->get('/room', [RoomsController::class, 'form']);
$router->post('/room', [RoomsController::class, 'save']);
$router->get('/room/delete', [RoomsController::class, 'delete']);

// Bookings
$router->get('/bookings', [BookingsController::class, 'list']);
$router->get('/booking', [BookingsController::class, 'form']);
$router->post('/booking', [BookingsController::class, 'save']);
$router->get('/booking/delete', [BookingsController::class, 'delete']);
$router->get('/booking/show', [BookingsController::class, 'show']);