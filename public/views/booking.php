<?php include('top.html'); ?>
        <h2><?=$booking->getId(); ?></h2>
        <div>
            <span class="label">Descrição:</span>
            <?=$booking->getDescription(); ?>
        </div>
        <div>
            <span class="label">Data inicial:</span>
            <?=Util::SqlDateToBr($booking->getDateIni()); ?>
        </div>
        <div>
            <span class="label">Data final:</span>
            <?=Util::SqlDateToBr($booking->getDateFim()); ?>
        </div>
<?php include('bottom.html'); ?>