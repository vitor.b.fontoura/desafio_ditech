<?php include('top.html'); ?>
        <h2>Usuários</h2>
        <div class='item' id='grid'>
        <table class="users" border="0" cellpadding="0" cellspacing="0">
            <thead>
                <tr>
                    <th style='width:50px;'><a href="/users?orderby=user_id">Id</a></th>
                    <th>Name<a href="/users?orderby=name" class="fr">^</a></th>
                    <th style='width:80px;'><a href="/users?orderby=username">Username</a></th>
                    <th><a href="/users?orderby=hash">Hash</a></th>
                    <th style='width:50px;'colspan=2>Ações</th>
                </tr>
            </thead>
            <tbody>
            <?php foreach ($users as $user) { ?>
                <tr>
                    <td><?=htmlentities($user->getId()); ?></td>
                    <td><?=htmlentities($user->getName()); ?></td>
                    <td><?=htmlentities($user->getUsername()); ?></td>
                    <td><?=htmlentities($user->getHash()); ?></td>
                    <td style='width:25px;'><a href="/user/delete?id=<?=$user->getId();?>"><img src="images/trashbin.png"></a></td>
                    <td style='width:25px;'><a href="/user?id=<?=$user->getId();?>"><img src="images/pencil.png"></a></td>
                </tr>
            <?php }?>
            </tbody>
        </table>
        </div>
<?php include('bottom.html'); ?>