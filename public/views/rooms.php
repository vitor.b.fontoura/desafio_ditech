<?php include('top.html'); ?>
        <h2>Salas</h2>
        <div class='item' id='grid'>
        <table class="users" border="0" cellpadding="0" cellspacing="0">
            <thead>
                <tr>
                    <th style='width:50px;'><a href="/rooms?orderby=id">Id</a></th>
                    <th><a href="/rooms?orderby=label">Sala</a></th>
                    <th><a href="/rooms?orderby=description">Descrição</a></th>
                    <th style='width:50px;'colspan=2>Ações</th>
                </tr>
            </thead>
            <tbody>
            <?php foreach ($rooms as $room) { ?>
                <tr>
                    <td><?=htmlentities($room->getId()); ?></td>
                    <td><?=htmlentities($room->getLabel()); ?></td>
                    <td><?=$room->getDescription(); ?></td>
                    <td style='width:25px;'><a href="/room/delete?id=<?=$room->getId();?>"><img src="images/trashbin.png"></a></td>
                    <td style='width:25px;'><a href="/room?id=<?=$room->getId();?>"><img src="images/pencil.png"></a></td>
                </tr>
            <?php }?>
            </tbody>
        </table>
        </div>
<?php include('bottom.html'); ?>