<?php include('top.html'); ?>
        <h2>Reservas</h2>
        <div class='item' id='grid'>
        <table class="users" border="0" cellpadding="0" cellspacing="0">
            <thead>
                <tr>
                    <th style='width:50px;'><a href="/bookings?orderby=id">Id</a></th>
                    <th><a href="/bookings?orderby=room_id">Sala</a></th>
                    <th><a href="/bookings?orderby=user_id">Usuário</a></th>
                    <th><a href="/bookings?orderby=description">Descrição</a></th>
                    <th style='width:150px;'><a href="/bookings?orderby=date_ini">Data/Hora Inicio</a></th>
                    <th style='width:150px;'><a href="/bookings?orderby=date_fim">Fim</a></th>
                    <th style='width:50px;'colspan=2>Ações</th>
                </tr>
            </thead>
            <?php foreach ($bookings as $booking) { ?>
                <tr>
                    <td><?=$booking->getId(); ?></td>
                    <td><?=$booking->getRoom()->getLabel(); ?></td>
                    <td><?=$booking->getUser()->getName(); ?></td>
                    <td><a href="/booking/show?id=<?=$booking->getId(); ?>"><?=$booking->getDescription(); ?></a></td>
                    <td><?=htmlentities(Util::SqlDateToBr($booking->getDateIni())); ?></td>
                    <td><?=htmlentities(Util::SqlDateToBr($booking->getDateFim())); ?></td>
                    <?php if ($booking->getUser()->getId() == $_SESSION['user_id']) {
                        ?>
                        <td style='width:25px;'><a href="/booking/delete?id=<?=$booking->getId();?>"><img src="images/trashbin.png"></a></td>
                        <td style='width:25px;'><a href="/booking?id=<?=$booking->getId();?>"><img src="images/pencil.png"></a></td>
                    <?php }else{ ?>
                        <td style='width:25px;'></td>
                        <td style='width:25px;'></td>
                    <?php } ?>
                </tr>
            <?php }?>
            </tbody>
        </table>
        </div>
<?php include('bottom.html'); ?>