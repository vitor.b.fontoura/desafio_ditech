<?php include('top.html'); ?>
        <?php
        if ($errors) {
            print '<ul class="errors">';
            foreach ( $errors as $field => $error ) {
                print '<li>'.htmlentities($error).'</li>';
            }
            print '</ul>';
        }
        ?>
        <h2><?=htmlentities($booking->getId() ? 'Editar Reserva' : 'Nova Reserva') ?></h2>
        <form method="POST" action="">
            <div class='item' style='max-width: 400px;'>
                <label for="room_id">Sala</label><br/>
                <select name="room_id">

                <?php
                foreach ($rooms as $room) {
                    ?><option value="<?=$room->getId(); ?>"><?=$room->getLabel(); ?></option><?php
                }
                ?>:
                </select>
            </div>
            <div class='item' style='max-width: 400px;'>
                <label for="date_ini">Data</label><br/>
                <input pattern="[0-9]{2}\/[0-9]{2}\/[0-9]{4}$" required type="datetime-local" name="date_ini" value="<?=str_replace(' ', 'T',$booking->getDateIni())?>"/>
            </div>
            <div class='item' style='max-width: 100%;'>
                <label for="description">Descrição</label><br/>
                <textarea required rows='3' type="text" name="description"><?=$booking->getDescription() ?></textarea>
            </div>
            <input type="hidden" name="form-submitted" value="1" />
            <div class='button-panel'>
                <input type="submit" value="Gravar"/>
                <input type="reset" value="Limpar"/>

                <div style='background:#b0bec5;' title='Campo sem validação'></div>
                <div style='background:#ffab91;' title='Campo Invalido'></div>
                <div style='background:#a5d6a7;' title='Campo correto'></div>
            </div>
        </form>
        
<?php include('bottom.html'); ?>
