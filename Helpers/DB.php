<?php

class DB
{
    private static $instance;

    public static function instance()
    {
        if (!static::$instance)
            static::$instance = new Database();

        return static::$instance;
    }
}