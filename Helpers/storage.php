<?php

if (!function_exists("public_path")) {
    function public_path($path)
    {
        return "public" . DIRECTORY_SEPARATOR . $path;
    }
}

if (!function_exists("view")) {
    function view($view_name, $args = null)
    {
        extract($args);
        include public_path("views" . DIRECTORY_SEPARATOR . $view_name . ".php");
    }
}